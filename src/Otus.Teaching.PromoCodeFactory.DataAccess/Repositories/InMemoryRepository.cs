﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data) => Data = data;

        public Task<IEnumerable<T>> GetAllAsync() => Task.FromResult(Data);

        public Task<T> GetByIdAsync(Guid id) => Task.FromResult(Data.FirstOrDefault(x => x.Id == id));

        public Task<Guid> Create(T data)
        {
            try
            {
                data.Id = Guid.NewGuid();
                Data = Data.Append(data);
                return Task.FromResult(data.Id);
            }
            catch (ArgumentNullException)
            {
                throw;
            }
        }

        public Task<bool> Delete(Guid id)
        {
            Data = Data.Where<T>(record => record.Id != id);
            return Task.FromResult(Data.Any(x => x.Id == id));
        }
        
        public Task<Guid> Update(T data)
        {
            try
            {
                Delete(data.Id);
                Create(data);    
                return Task.FromResult(data.Id);        
            }
            catch (InvalidOperationException)
            {
                throw;
            }
        }
    }
}
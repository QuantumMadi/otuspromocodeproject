﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set;}
        protected BaseEntity()
        {
            Id = Guid.NewGuid();
        }
    }
}